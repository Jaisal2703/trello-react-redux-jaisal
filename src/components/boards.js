import React, { Component } from 'react';
import Board from './board';
const token =
  'cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1';
const key = '6b3f8447fcbcae69f0a8b179cf2d6cad';

class Boards extends Component {
  state = {
    boards: []
  };
  componentDidMount() {
    fetch(
      `https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`,
      {
        method: 'GET'
      }
    )
      .then(data => data.json())
      .then(data => {
        // console.log(data);
        this.setState({
          boards: data
        });
      });
  }
  render() {
    if (this.state.boards.length === 0) {
      return (
        <h1 className="multicolortext" style={{ fontSize: '200px' }}>
          Loading...
        </h1>
      );
    }
    var allBoards = this.state.boards.map(board => {
      return <Board key={board.id} boards={board} />;
    });
    return allBoards;
  }
}

export default Boards;
