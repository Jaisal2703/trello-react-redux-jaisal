import React, { Component } from 'react';
import Form from './Form';
import CheckItem from './CheckItem'; //component of checkItem

const token =
  'cf785635a4a4a13b7183918f3e35f5847c1376f4de777744303ebe4964afd5f1';
const key = '6b3f8447fcbcae69f0a8b179cf2d6cad';

class CheckList extends Component {
  state = {
    checkItems: []
  };
  componentDidMount() {
    fetch(
      `https://api.trello.com/1/checklists/${this.props.checkList.id}/checkItems?key=${key}&token=${token}`,
      {
        method: 'GET'
      }
    )
      .then(data => data.json())
      .then(data =>
        this.setState({
          checkItems: data
        })
      );
  }
  openHideDiv = () => {
    this.setState({
      hideDiv: true
    });
  };
  closeInputDiv = () => {
    this.setState({
      hideDiv: false
    });
  };
  inputState = event => {
    this.setState({
      inputValue: event.target.value
    });
  };
  addNewCheckItem = () => {
    fetch(
      `https://api.trello.com/1/checklists/${this.props.checkList.id}/checkItems?name=${this.state.inputValue}&pos=bottom&checked=false&key=${key}&token=${token}`,
      {
        method: 'POST'
      }
    )
      .then(data => data.json())
      .then(data =>
        this.setState({
          checkItems: this.state.checkItems.concat([data]),
          inputValue: ''
        })
      );
      this.closeInputDiv()
  };
  deleteCheckItem = id => {
    fetch(
      `https://api.trello.com/1/checklists/${this.props.checkList.id}/checkItems/${id}?key=${key}&token=${token}`,
      {
        method: 'DELETE'
      }
    ).then(() => {
      this.setState({
        checkItems: this.state.checkItems.filter(
          CheckItem => CheckItem.id !== id
        )
      });
    });
  };
  updateCheckItem = (event, checkItem) => {
    // console.log("event",event)
    var checkItemStatus = event.target.checked ? 'complete' : 'incomplete';
    fetch(
      `https://api.trello.com/1/cards/${this.props.checkList.idCard}/checkItem/${checkItem.id}?state=${checkItemStatus}&key=${key}&token=${token}`,
      {
        method: 'PUT'
      }
    )
      .then(data => data.json())
      .then(data => {
        var allItem = this.state.checkItems;
        allItem[allItem.indexOf(checkItem)].state = data.state;
        this.setState({
          checkItems: allItem
        });
      });
  };
  render() {
    //console.log(this.props.checkList);
    var closeaddButton = this.state.hideDiv ? 'none' : 'block';
    var openHideDiv = this.state.hideDiv ? 'block' : 'none';
    let checkItems = this.state.checkItems.map(checkItem => (
      <CheckItem
        key={checkItem.id}
        checkItem={checkItem}
        deleteCheckItem={this.deleteCheckItem}
        updateCheckItem={this.updateCheckItem}
      />
    ));
    return (
      <div className="checkListContainer">
        <h3 className="check">{this.props.checkList.name}</h3>
        <div className="itemsContainer">{checkItems}</div>
        <div className="buttonsOfCheckList">
          <div>
            <button
              onClick={() =>
                this.props.deleteCheckList(this.props.checkList.id)
              }
              className="deleteButtonForCheckList btn btn-danger btn-xsm"
            >
              Delete
            </button>
          </div>
          <div>
            <button
              onClick={this.openHideDiv}
              style={{ display: closeaddButton }}
              className="addButtonForCheckItem btn btn-primary btn-xsm"
            >
              Add items
            </button>
            <Form
              style={{ display: openHideDiv }}
              closeInputDiv={this.closeInputDiv}
              inputState={this.inputState}
              input={this.state.inputValue}
              addNewCard={this.addNewCheckItem}
              buttonTitle="check item"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default CheckList;
